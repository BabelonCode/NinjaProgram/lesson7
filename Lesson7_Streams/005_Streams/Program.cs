﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream stream = new FileStream("test.txt", FileMode.Open, FileAccess.Read);

            foreach (byte item in stream.AsEnumerable())
            {

            }

            //foreach (var item in stream.Lines())
            //{
            //    Console.WriteLine(ToString(item));
            //}

            stream.Dispose();

            Console.ReadLine();
        }

        static string ToString(List<byte> source)
        {
            var builder = new StringBuilder(source.Count);
            foreach (var item in source)
            {
                builder.Append((char)item);
            }
            return builder.ToString();
        }
    }
}
