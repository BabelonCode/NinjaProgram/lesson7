﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream stream = new FileStream("test.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

            for (int i = 0; i < 256; i++)
            {
                stream.WriteByte((byte)i);
            }

            stream.Position = 0;

            for (int i = 0; i < 256; i++)
            {
                Console.Write(stream.ReadByte() + " ");
            }

            stream.Dispose();

            Console.ReadLine();
        }
    }
}
