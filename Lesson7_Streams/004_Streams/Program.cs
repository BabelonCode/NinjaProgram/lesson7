﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _004_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream stream = new FileStream("test.txt", FileMode.Open, FileAccess.Read);

            foreach (byte item in stream.AsEnumerable())
            {

            }

            //stream.Position = 0;

            //foreach (List<byte> item in stream.AsEnumerable().ToLines())
            //{

            //}

            stream.Dispose();

            Console.ReadLine();
        }
    }
}
