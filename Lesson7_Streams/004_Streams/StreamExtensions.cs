﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _004_Streams
{
    static class StreamExtensions
    {
        public static IEnumerable<byte> AsEnumerable(this Stream source)
        {
            int symbol;
            while ((symbol = source.ReadByte()) != -1)
            {
                yield return (byte)symbol;
            }
        }

        public static IEnumerable<List<byte>> ToLines(this IEnumerable<byte> source)
        {
            var list = new List<byte>();
            foreach (byte item in source)
            {
                // \r
                if (item == 13)
                {
                    yield return list;
                    list = new List<byte>();
                }
                // \n
                else if (item != 10)
                    list.Add(item);
            }
        }
    }
}
