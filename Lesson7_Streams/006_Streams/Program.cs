﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            var memory = new MemoryStream();

            for (int i = 0; i < 256; i++)
            {
                memory.WriteByte((byte)i);
            }

            for (int i = 0; i < 10; i++)
            {
                memory.WriteByte((byte)i);
            }

            memory.Position = 0;

            int symbol;
            while ((symbol = memory.ReadByte()) != -1)
            {
                Console.Write(symbol + " ");
            }

            memory.Dispose();

            Console.ReadLine();
        }
    }
}
