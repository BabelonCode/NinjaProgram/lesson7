﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            var stream = new FileStream("test.txt", FileMode.Create, FileAccess.ReadWrite);

            var writer = new StreamWriter(stream);

            writer.WriteLine("Hello, world!");
            writer.WriteLine("Hello, world!");
            writer.WriteLine("Hello, world!");
            writer.WriteLine("Hello, world!");

            writer.Dispose();

            var reader = File.OpenText("test.txt");

            string input;
            while ((input = reader.ReadLine()) != null)
            {
                Console.WriteLine(input);
            }

            reader.Dispose();
            
            Console.ReadLine();
        }
    }
}
