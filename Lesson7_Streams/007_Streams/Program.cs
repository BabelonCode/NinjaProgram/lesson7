﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            var memory = new MemoryStream();

            for (int i = 0; i < 256; i++)
            {
                memory.WriteByte((byte)i);
            }

            var stream = new FileStream("test.txt", FileMode.Create, FileAccess.ReadWrite);
            memory.WriteTo(stream);

            stream.Position = 0;

            int symbol;
            while ((symbol = stream.ReadByte()) != -1)
            {
                Console.Write(symbol + " ");
            }

            memory.Dispose();
            stream.Dispose();

            Console.ReadLine();
        }
    }
}
