﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream stream = new FileStream("test.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

            for (int i = 0; i < 256; i++)
            {
                stream.WriteByte((byte)i);
            }

            stream.Position = 0;

            int symbol;
            while ((symbol = stream.ReadByte()) != -1)
            {
                Console.Write(symbol + " ");
            }

            stream.Dispose();

            Console.ReadLine();
        }
    }
}
