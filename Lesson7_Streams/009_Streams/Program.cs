﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            var stream = new FileStream("test.txt", FileMode.Create, FileAccess.ReadWrite);

            BufferedStream buffered = new BufferedStream(stream);
        }
    }
}
