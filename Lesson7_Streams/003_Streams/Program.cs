﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream stream = new FileStream("test.txt", FileMode.Open, FileAccess.Read);

            int symbol;
            while ((symbol = stream.ReadByte()) != -1)
            {
                Console.Write((char)symbol);
            }

            stream.Dispose();
            Console.ReadLine();
        }
    }
}
